﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboMultiplier : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(Powerups.instance.DoubleMultiplier());
        Destroy(gameObject);
    }
}
