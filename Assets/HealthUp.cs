﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUp : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Powerups.instance.HealthUp();
        Destroy(gameObject);
    }
}
