﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shake = 0f, shakeAmount = 0.7f, decreaseFactor = 1.0f;
    public Vector3 startPos;

    private void Awake()
    {
        startPos = Camera.main.transform.localPosition;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            shake = 1.0f;
        }

        if(shake > 0)
        {
            Camera.main.transform.localPosition = (Random.insideUnitSphere * shakeAmount);
            shake -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shake = 0f;
            Camera.main.transform.localPosition = startPos;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(Camera.main.transform.position, shakeAmount);
    }
}