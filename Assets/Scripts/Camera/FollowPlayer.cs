﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Vector3 offset;
    public GameObject player;
    public float sensitivity;

    public float minZoom;
    public float maxZoom;
	
	void Update ()
    {
        transform.position = player.transform.position + offset;
        if(Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            var input = offset.y;
            input -= (Input.GetAxis("Mouse ScrollWheel") * sensitivity);
            input = Mathf.Clamp(input, minZoom, maxZoom);
            offset.y = input;
        }
	}
}
