﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerType : MonoBehaviour
{
    public Text typeArea;
    public string userInput = string.Empty;

    private void Awake()
    {
        typeArea.text = string.Empty;
    }

    private void Update()
    {
        if(Input.anyKeyDown)
        {
            if(!string.IsNullOrEmpty(Input.inputString))
            {
                HasTyped();
            }
        }
    }

    void HasTyped()
    {
        for(int i = 0; i < Input.inputString.Length; i++)
        {
            var ch = Input.inputString[i];
            if (ch == '\b')
            {
                if(userInput.Length >= 1)
                {
                    userInput = userInput.Substring(0, userInput.Length - 1);
                }
            }
            else if (ch == '\n' || ch == '\r')
            {
                GlobalEvents.instance.WordCheck(userInput);
                userInput = string.Empty;
            }
            else
            {
                userInput += ch;
            }
        }
        if(typeArea != null)
        {
            typeArea.text = userInput;
        }
    }
}
