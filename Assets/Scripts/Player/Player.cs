﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int health = 100;

    void TakeDamage(int damage)
    {
        health -= damage;
        StartCoroutine(UIHandler.instance.DamageHealth(health));
        ScoreHandler.instance.ResetCombo();
        GlobalEvents.instance.shake = 1f;
        AudioHandler.instance.PlayDamage();
        if(health <= 0)
        {
            GlobalEvents.instance.GameOver();
        }
    }
}
