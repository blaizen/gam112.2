﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GIFLooper : MonoBehaviour
{
    public List<Texture> frames = new List<Texture>();
    int fps = 40;

     RawImage raw;

    private void Awake()
    {
        raw = GetComponent<RawImage>();
    }

    private void Update()
    {
        int index = (int)((Time.time * fps) % frames.Count);
        raw.texture = frames[index];
    }
}
