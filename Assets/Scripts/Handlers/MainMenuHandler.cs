﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuHandler : MonoBehaviour
{

    public static GameObject mainMenu;
    public AudioSource BGM;

    private void Awake()
    {
        mainMenu = GameObject.Find("MainMenuCanvas");
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void CreditsMenu()
    {
        SceneManager.LoadScene("Credits", LoadSceneMode.Additive);
        mainMenu.SetActive(false);
    }

    public static void ReturnToMenu(string scene)
    {
        SceneManager.UnloadSceneAsync(scene);
        mainMenu.SetActive(true);
    }
}
