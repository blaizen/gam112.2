﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsMenuHandler : MonoBehaviour
{
    public void BackToMenu()
    {
        MainMenuHandler.ReturnToMenu("Credits");
    }
}
