﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemHandler : MonoBehaviour
{
    public static ParticleSystemHandler instance;

    [Header("Particle System Prefabs")]
    public GameObject explosion;

    private void Awake()
    {
        instance = this;
    }

    public void Explosion(Vector3 loc)
    {
        GameObject instantiatedExplosion = Instantiate(explosion, loc, Quaternion.identity);
        Destroy(instantiatedExplosion, 0.5f);
    }
}
