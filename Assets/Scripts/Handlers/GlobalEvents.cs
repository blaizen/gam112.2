﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalEvents : MonoBehaviour
{
    public static GlobalEvents instance;
    GameObject player;

    public int longwordLetterCount = 7;

    public float shake = 0f;
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 5f;
    Vector3 startPos;

    public int powerupChance;
    public GameObject comboMultiplier;
    public GameObject healthUp;

    private void Awake()
    {
        instance = this;
        Time.timeScale = 1f;
        player = GameObject.Find("Player");
        startPos = Camera.main.transform.localPosition;
    }

    private void Update()
    {
        if (shake > 0)
        {
            Camera.main.transform.localPosition = (Random.insideUnitSphere * shakeAmount);
            shake -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shake = 0f;
            Camera.main.transform.localPosition = startPos;
        }
    }

    public List<EnemyWordSelector> enemies = new List<EnemyWordSelector>();

    public void AddEnemy(EnemyWordSelector enemy)
    {
        enemies.Add(enemy);
    }

    public void RemoveEnemy(EnemyWordSelector enemy)
    {
        enemies.Remove(enemy);
    }

    public void WordCheck(string word)
    {
        int numberOfHits = 0;
        for(int i = 0; i < enemies.Count; i++)
        {
            bool sameWord = enemies[i].CompareWord(word);
            if(sameWord)
            {
                numberOfHits++;
            }
        }
        if(numberOfHits == 0)
        {
            StartCoroutine(UIHandler.instance.NoWordFound());
            ScoreHandler.instance.ResetCombo();
            player.SendMessage("TakeDamage", 5f);
            AudioHandler.instance.PlayDamage();
        }
        else
        {
            ScoreHandler.instance.AddWordTyped(numberOfHits);
            bool longword = false;
            if(word.Length > longwordLetterCount)
            {
                longword = true;
            }
            AudioHandler.instance.PlayWordSound(longword, ScoreHandler.instance.comboCount);
        }
    }

    public void PickupCheck(Vector3 loc)
    {
        int rand = Random.Range(0, 100);
        if (rand < powerupChance)
        {
            GlobalEvents.instance.DropPickup(loc);
        }
    }

    public void DropPickup(Vector3 loc)
    {
        int rand = Random.Range(1,3);
        switch (rand)
        {
            case 1:
                Instantiate(comboMultiplier, loc, Quaternion.identity);
                break;
            case 2:
                Instantiate(healthUp, loc, Quaternion.identity);
                break;
            default:
                break;
        }
    }

    public void GameOver()
    {
        AudioHandler.instance.AudioGameOver();
        PlayerPrefs.SetFloat("WPM", ScoreHandler.instance.wpm);
        PlayerPrefs.SetFloat("Score", ScoreHandler.instance.score);
        Destroy(player.GetComponent<PlayerType>());
        Destroy(player.GetComponent<PlayerMovement>());
        Time.timeScale = 0;
        SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
    }
}
