﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreenHandler : MonoBehaviour
{
    public Text wpmText;
    public Text scoreText;

    private void Awake()
    {
        wpmText.text = PlayerPrefs.GetFloat("WPM").ToString();
        scoreText.text = PlayerPrefs.GetFloat("Score").ToString();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
