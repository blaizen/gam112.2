﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnHandler : MonoBehaviour
{
    public GameObject enemy;
    public GameObject player;
    public float randLocDist;

    public float closeProximity;
    bool newGame;
    int enemyCount = 0;
    public int increaseDifficultyCount = 5;
    public float newGameCountDown = 5f;
    float spawnTimer = 2f;

    private void Awake()
    {
        newGame = true;
    }

    private void Update()
    {
        if(newGame)
        {
            newGameCountDown -= Time.deltaTime;
            UIHandler.instance.UpdateCountDown(Mathf.Round(newGameCountDown));
            if(newGameCountDown <= 0f)
            {
                newGame = false;
                StartCoroutine(SpawnEnemy());
            }
        }
    }

    IEnumerator SpawnEnemy()
    {
        Debug.Log("COROUTINE STARTING");
        float randX = Random.Range(transform.position.x - randLocDist, transform.position.x + randLocDist);
        float randZ = Random.Range(transform.position.z - randLocDist, transform.position.z + randLocDist);
        Vector3 randPos = new Vector3(randX, transform.position.y, randZ);

        if(Vector3.Distance(randPos, player.transform.position) < closeProximity)
        {
            Debug.Log("TOO CLOSE TO PLAYER");
            yield return null;
        }
        Debug.Log("CHECKING NAVMESH");
        NavMeshHit hit;
        if(NavMesh.SamplePosition(randPos, out hit, randLocDist, -1))
        {
            randPos = hit.position;
        }
        else
        {
            Debug.Log("UNABLE TO FIND LOCATION");
            yield return null;
        }

        Instantiate(enemy, randPos, Quaternion.identity);
        yield return new WaitForSeconds(spawnTimer);
        enemyCount++;

        //Difficult increases over time.
        if(enemyCount == increaseDifficultyCount)
        {
            enemyCount = 0;
            if(spawnTimer > 0.1f)
            {
                increaseDifficultyCount += 3;
                spawnTimer -= 0.1f;
            }
            Debug.Log("Spawn timer decreased! Now: " + spawnTimer + " seconds");
        }
        StartCoroutine(SpawnEnemy());
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, randLocDist);
    }
}
