﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    public static UIHandler instance;

    public RectTransform typeAreaPanel;
    public RectTransform healthPanel;
    public Text healthText;
    public Text countdownText;
    float countdownTemp = 0;

    private void Awake()
    {
        instance = this;
    }

    public IEnumerator NoWordFound()
    {
        Image img = typeAreaPanel.GetComponent<Image>();
        Color current = img.color;
        img.color = new Color(Color.red.r, Color.red.g, Color.red.b, current.a);
        yield return new WaitForSeconds(0.5f);
        img.color = current;
    }

    public IEnumerator DamageHealth(int health)
    {
        Image img = healthPanel.GetComponent<Image>();
        Color current = img.color;
        img.color = new Color(Color.red.r, Color.red.g, Color.red.b, current.a);
        healthText.text = ("HP: " + health.ToString());
        yield return new WaitForSeconds(0.5f);
        img.color = current;
    }

    public IEnumerator GainHealth(int health)
    {
        Image img = healthPanel.GetComponent<Image>();
        Color current = img.color;
        img.color = new Color(Color.green.r, Color.green.g, Color.green.b, current.a);
        healthText.text = ("HP: " + health.ToString());
        yield return new WaitForSeconds(0.5f);
        img.color = current;
    }

    public void UpdateCountDown(float time)
    {
        //Countdown temp is used for audio purposes. It detects when the time has gone down and plays the blipping noise used in the countdown.
        if(countdownTemp == 0)
        {
            countdownTemp = time;
        }

        if(countdownTemp != time)
        {
            countdownTemp = time;
            if (countdownTemp <= 0)
            {
                AudioHandler.instance.PlayCountDownSound(countdownTemp);
            }
            else
            {
                AudioHandler.instance.PlayCountDownSound(countdownTemp);
            }
        }
        if(time == 0)
        {
            countdownText.text = "GO!";
            Invoke("RemoveCountDown", 1f);
        }
        else
        {
            countdownText.text = time.ToString();
        }
    }

    void RemoveCountDown()
    {
        countdownText.gameObject.SetActive(false);
    }
}
