﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerups : MonoBehaviour
{
    public static Powerups instance;

    public float powerupTimer = 10f;

    public int healAmount;

    private void Awake()
    {
        instance = this;
    }

    public IEnumerator DoubleMultiplier()
    {
        ScoreHandler.instance.comboCount *= 2;
        yield return new WaitForSeconds(powerupTimer);
        ScoreHandler.instance.comboCount /= 1;
    }

    public void HealthUp()
    {
        GameObject player = GameObject.Find("Player");
        Player playerScript = player.GetComponent<Player>();
        playerScript.health += healAmount;
        StartCoroutine(UIHandler.instance.GainHealth(playerScript.health));
    }
}
