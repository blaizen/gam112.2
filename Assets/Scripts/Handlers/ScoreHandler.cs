﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreHandler : MonoBehaviour
{
    public static ScoreHandler instance;

    public Text wpmText;
    public Text scoreText;
    public Text comboText;
    public RawImage fireAnim;

    int standardFontSize;
    public int maxFontSize = 55;

    int wordsTyped = 0;
    public float score = 0;
    public float wpm;
    public int comboCount = 1;
    public float comboMaxTime;
    bool combo = false;
    float comboTimer = 0;

    private void Awake()
    {
        instance = this;
        standardFontSize = comboText.fontSize;
    }

    public void AddWordTyped(int hits)
    {
        wordsTyped += hits;
        score += (hits * comboCount);
        combo = true;
        comboTimer = 0;
        comboCount++;
        comboText.color = new Color(comboText.color.r, comboText.color.g - (comboCount / 100), comboText.color.b - (comboCount / 100));
        //Converts combo count to a float to use with the fire GIF. It was needed as int in a different script.
        float floatCombo = comboCount;
        fireAnim.color = new Color(fireAnim.color.r, fireAnim.color.g, fireAnim.color.b, fireAnim.color.a + (floatCombo / 20));
        comboText.fontSize++;
        comboText.fontSize = Mathf.Clamp(comboText.fontSize, standardFontSize, maxFontSize);
        UpdateScore();
    }

    void UpdateScore()
    {
        wpm = ((wordsTyped * 60) / Time.timeSinceLevelLoad);
        wpm = Mathf.Round(wpm);
        wpmText.text = ("WPM: " + wpm.ToString());
        scoreText.text = ("Score: " + score.ToString());
        comboText.text = (comboCount + "x");
    }

    private void Update()
    {
        UpdateScore();
        if(combo)
        {
            comboTimer += Time.deltaTime;
            if(comboTimer > comboMaxTime)
            {
                ResetCombo();
            }
        }
    }

    public void ResetCombo()
    {
        //Change BGM
        AudioHandler.instance.LoseCombo();

        combo = false;
        comboTimer = 0;
        comboCount = 1;
        comboText.color = Color.white;
        fireAnim.color = new Color(fireAnim.color.r, fireAnim.color.g, fireAnim.color.b, 0);
        comboText.fontSize = standardFontSize;
    }
}
