﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    public static AudioHandler instance;

    //Seperated to make it neat in the Unity inspector.
    [Header("In-Game Sounds")]
    public AudioClip short1x;
    public AudioClip short2x, short3x, short4x, short5x, short6x, short7x, short8x, short9x, short10x, longword, wrongword, countdown, countdownGo;
    public AudioSource soundPlayer, explosionPlayer, bgmPlayer, bgmCombo;

    [Header("Menu Sounds")]
    public AudioSource menuBgm;

    [Header("Music Fade Amount")]
    public float fade;

    bool comboBGMPlaying = false;
    bool countdownGoFired = false;

    private void Awake()
    {
        instance = this;
    }

    public void PlayWordSound(bool isLongWord, int combo)
    {
        if(isLongWord)
        {
            soundPlayer.PlayOneShot(longword);
        }
        else
        {
            switch(combo)
            {
                case 1:
                    soundPlayer.PlayOneShot(short1x);
                    break;
                case 2:
                    soundPlayer.PlayOneShot(short2x);
                    break;
                case 3:
                    soundPlayer.PlayOneShot(short3x);
                    break;
                case 4:
                    soundPlayer.PlayOneShot(short4x);
                    break;
                case 5:
                    soundPlayer.PlayOneShot(short5x);
                    comboBGMPlaying = true;
                    StartCoroutine(FadeOut(bgmPlayer, fade));
                    StartCoroutine(FadeIn(bgmCombo, fade));
                    break;
                case 6:
                    soundPlayer.PlayOneShot(short6x);
                    break;
                case 7:
                    soundPlayer.PlayOneShot(short7x);
                    break;
                case 8:
                    soundPlayer.PlayOneShot(short8x);
                    break;
                case 9:
                    soundPlayer.PlayOneShot(short9x);
                    break;
                case 10:
                    soundPlayer.PlayOneShot(short10x);
                    break;
                default:
                    soundPlayer.PlayOneShot(short10x);
                    break;
            }
        }
    }

    public void PlayDamage()
    {
        soundPlayer.PlayOneShot(wrongword);
    }

    public void PlayCountDownSound(float timer)
    {
        if(timer == 0)
        {
            soundPlayer.PlayOneShot(countdownGo);
            bgmPlayer.Play();
        }
        else
        {
            soundPlayer.PlayOneShot(countdown);
        }
    }

    public void PlayExplosion(Vector3 loc)
    {
        AudioSource explosionClone = Instantiate(explosionPlayer, loc, Quaternion.identity);
    }

    public void LoseCombo()
    {
        //Attempts to check what BGM music is playing to determine if we need to fade.
        if(comboBGMPlaying)
        {
            comboBGMPlaying = false;
            StartCoroutine(FadeOut(bgmCombo, fade));
            StartCoroutine(FadeIn(bgmPlayer, fade));
        }
    }

    public void AudioGameOver()
    {
        if(comboBGMPlaying)
        {
            StartCoroutine(FadeOut(bgmCombo, fade));
        }
        else
        {
            StartCoroutine(FadeOut(bgmPlayer, fade));
        }
    }

    public IEnumerator FadeOut(AudioSource audio, float fadeTime)
    {
        while (audio.volume > 0)
        {
            audio.volume -= 0.8f * Time.deltaTime / fadeTime;

            yield return null;
        }
        audio.Stop();
    }

    public IEnumerator FadeIn(AudioSource audio, float fadeTime)
    {
        audio.volume = 0f;
        audio.Play();
        while (audio.volume < 0.8)
        {
            audio.volume += 0.8f * Time.deltaTime / fadeTime;

            yield return null;
        }
    }
}
