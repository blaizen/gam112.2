﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    NavMeshAgent agent;
    GameObject player;

    public float maxDist;
    public float visionDistance;

    enum States
    {
        Idle,
        Move,
        Attack
    }


    States states;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Player");
        states = States.Idle;
    }

    private void Update()
    {
        switch(states)
        {
            case States.Idle:
                Idle();
                break;
            case States.Move:
                Move();
                break;
            case States.Attack:
                Attack();
                break;
            default:
                break;
        }
    }

    void Idle()
    {
        agent.isStopped = true;
        if(Vector3.Distance(transform.position, player.transform.position) < visionDistance)
        {
            states = States.Move;
        }
    }

    void Move()
    {
        agent.isStopped = false;
        agent.destination = player.transform.position;
        if (Vector3.Distance(transform.position, player.transform.position) < maxDist)
        {
            states = States.Attack;
        }
        if (Vector3.Distance(transform.position, player.transform.position) > visionDistance)
        {
            states = States.Idle;
        }
    }

    void Attack()
    {
        agent.isStopped = true;
        SendMessage("DamagePlayer");
        if (Vector3.Distance(transform.position, player.transform.position) > maxDist && Vector3.Distance(transform.position, player.transform.position) < visionDistance)
        {
            states = States.Move;
        }
        else if (Vector3.Distance(transform.position, player.transform.position) > maxDist && Vector3.Distance(transform.position, player.transform.position) > visionDistance)
        {
            states = States.Idle;
        }
    }
}
