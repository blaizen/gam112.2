﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Attacking Attributes")]
    public int damage;
    bool canDamage = true;
    public float damageCoolDown;
    public float attackDistance;
    float timer = 0;
    GameObject player;

    private void Awake()
    {
         player = GameObject.Find("Player");
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if(timer > damageCoolDown)
        {
            canDamage = true;
        }

        if(Vector3.Distance(transform.position, player.transform.position) < attackDistance)
        {
            DamagePlayer();
        }
    }

    void DamagePlayer()
    {
        if(canDamage)
        {
            player.SendMessage("TakeDamage", damage);
            timer = 0;
            canDamage = false;
        }
    }
}
