﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class EnemyWordSelector : MonoBehaviour
{
    public TextAsset textData;
    public List<string> words = new List<string>();

    private readonly char[] WORD_SEPERATOR = new char[] { '\n', ' ', '\r' };

    public Text myWord;
    private Quaternion myWordRot;

    private void Awake()
    {
        var stringArray = textData.text.Split(WORD_SEPERATOR);
        words = stringArray.ToList();

        words.RemoveAll(x => string.IsNullOrEmpty(x));

        myWord.text = GetWord();
    }

    private void Start()
    {
        GlobalEvents.instance.AddEnemy(this);
        myWordRot = myWord.transform.rotation;
    }

    private void Update()
    {
        myWord.transform.rotation = myWordRot;
    }

    public string GetWord()
    {
        int randWord = Random.Range(0, words.Count);
        return words[randWord];
    }

    public bool CompareWord(string typed)
    {
        if(myWord.text == typed)
        {
            myWord.text = GetWord();
            ParticleSystemHandler.instance.Explosion(transform.position);
            AudioHandler.instance.PlayExplosion(transform.position);
            GlobalEvents.instance.PickupCheck(transform.position);
            Destroy(gameObject);
            return true;
        }
        return false;
    }

    private void OnDestroy()
    {
        GlobalEvents.instance.RemoveEnemy(this);
    }
}
